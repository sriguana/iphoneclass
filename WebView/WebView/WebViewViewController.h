//
//  WebViewViewController.h
//  WebView
//
//  Created by iPhone on 5/25/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewViewController : UIViewController {
    IBOutlet UIWebView *webView;
    IBOutlet UIProgressView *barra;
    // contenemos para cargas async
    NSMutableData * datos;
    NSURLConnection * coneccion;
    
    float total, parcial;
    NSURLResponse * respuesta;
}

@end
