//
//  WebViewViewController.m
//  WebView
//
//  Created by iPhone on 5/25/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "WebViewViewController.h"

@implementation WebViewViewController

- (void)dealloc
{
    [webView release];
    [barra release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // NSString * dir = @"http://www.nubenueve.com/iphone/catalogo.pdf";
    // NSString * dir = @"http://www.nubenueve.com/iphone/guideline.pdf";
    NSString * dir = @"http://www.nubenueve.com/iphone/book.pdf";
    NSURL * url = [NSURL URLWithString:dir];
    NSURLRequest * request = [NSURLRequest requestWithURL:url];
    datos = [[NSMutableData alloc] retain];
    respuesta = [[NSURLResponse alloc] init];
    coneccion = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    // inicia la descarga de datos
    [coneccion start];
}

// al inicio de la descarga de datos
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    total = [response expectedContentLength];
    respuesta = [response copy];
}

// mientras se ejcuta la carga de datos
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // guardamos la data en un contenedor
    [datos appendData:data];
    parcial = [datos length];
    [barra setProgress:(parcial / total)];
    UILabel * porcentaje = (UILabel *)[self.view viewWithTag:5];
    // float sin decimales
    porcentaje.text = [NSString stringWithFormat:@"%.0f%@", barra.progress * 100, @"%"];
}

// al finalizar la descarga
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // se presenta la informacion
    [webView loadData:datos MIMEType:[respuesta MIMEType] textEncodingName:[respuesta textEncodingName] baseURL:[respuesta URL]];
    // envia la vista al frente
    [self.view bringSubviewToFront:webView];
}


- (void)viewDidUnload
{
    [webView release];
    webView = nil;
    [barra release];
    barra = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

@end
