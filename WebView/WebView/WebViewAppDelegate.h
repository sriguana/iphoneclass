//
//  WebViewAppDelegate.h
//  WebView
//
//  Created by iPhone on 5/25/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WebViewViewController;

@interface WebViewAppDelegate : NSObject <UIApplicationDelegate> {

}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain) IBOutlet WebViewViewController *viewController;

@end
