//
//  OchoViewController.m
//  Ocho
//
//  Created by iPhone on 5/8/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "OchoViewController.h"

@implementation OchoViewController

- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Nueva instancia de la clase TablaViewCotnroller
    TablaViewController * Tabla = [[TablaViewController alloc] initWithNibName:@"TablaViewController" bundle:nil];
    
    // Asignamos un array con la nueva tabla al conjunto de controladores manejados por el controlador principal.
    self.viewControllers = [NSArray arrayWithObject: Tabla];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
