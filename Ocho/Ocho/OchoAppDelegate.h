//
//  OchoAppDelegate.h
//  Ocho
//
//  Created by iPhone on 5/8/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class OchoViewController;

@interface OchoAppDelegate : NSObject <UIApplicationDelegate> {

}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain) IBOutlet OchoViewController *viewController;

@end
