//
//  FotosViewController.m
//  Fotos
//
//  Created by iPhone on 5/17/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "FotosViewController.h"

@implementation FotosViewController

- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIScrollView *scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0,0,320,480)];
    
    float ancho = 0;
    int num = 8;
    for (int i=0; i<num; i++) {
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(320*i,0,320,480)];
        img.image = [UIImage imageNamed:[NSString stringWithFormat:@"%i.jpg", i]];
        
        img.contentMode = UIViewContentModeScaleAspectFit;
        
        [scroll addSubview:img];
        [img release];
        ancho+=320;
    }
    
    scroll.contentSize = CGSizeMake(ancho, 0);
    
    scroll.pagingEnabled = YES;
    
    [self.view addSubview:scroll];
    [scroll release];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
