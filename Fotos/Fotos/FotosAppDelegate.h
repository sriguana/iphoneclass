//
//  FotosAppDelegate.h
//  Fotos
//
//  Created by iPhone on 5/17/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FotosViewController;

@interface FotosAppDelegate : NSObject <UIApplicationDelegate> {

}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain) IBOutlet FotosViewController *viewController;

@end
