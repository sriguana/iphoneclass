//
//  AudioPlayerAppDelegate.h
//  AudioPlayer
//
//  Created by iPhone on 5/15/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AudioPlayerViewController;

@interface AudioPlayerAppDelegate : NSObject <UIApplicationDelegate> {

}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain) IBOutlet AudioPlayerViewController *viewController;

@end
