//
//  AudioPlayerViewController.m
//  AudioPlayer
//
//  Created by iPhone on 5/15/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "AudioPlayerViewController.h"

@implementation AudioPlayerViewController

- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // ruta absoluta del archivo dentro de la app
    NSString * path = [[NSBundle mainBundle] pathForResource:@"caruso" ofType:@"aif"];
    
    NSError *error;
    
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:path] error:&error]; 
}

-(UIButton *)botonPlay {
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)botonPlay:(id)sender {
    [player play];
}

- (IBAction)botonPlayStop:(id)sender {
    [player stop];
}

- (IBAction)botonStop:(id)sender {
    [player stop];
}
@end
