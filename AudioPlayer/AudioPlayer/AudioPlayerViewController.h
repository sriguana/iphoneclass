//
//  AudioPlayerViewController.h
//  AudioPlayer
//
//  Created by iPhone on 5/15/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface AudioPlayerViewController : UIViewController {
    
    AVAudioPlayer *player;

}

- (IBAction)botonPlay:(id)sender;
- (IBAction)botonPlayStop:(id)sender;
- (IBAction)botonStop:(id)sender;

@end
