//
//  WeViewAppDelegate.h
//  WeView
//
//  Created by iPhone on 5/25/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WeViewViewController;

@interface WeViewAppDelegate : NSObject <UIApplicationDelegate> {

}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain) IBOutlet WeViewViewController *viewController;

@end
