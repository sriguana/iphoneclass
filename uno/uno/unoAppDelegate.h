//
//  unoAppDelegate.h
//  uno
//
//  Created by iPhone on 4/24/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class unoViewController;

@interface unoAppDelegate : NSObject <UIApplicationDelegate> {

}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain) IBOutlet unoViewController *viewController;

@end
