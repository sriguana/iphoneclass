//
//  unoViewController.h
//  uno
//
//  Created by iPhone on 4/24/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface unoViewController : UIViewController {
    
    
    // Declarando variables - por defecto son privados - solo implementación no asignación.
    // Toda variable declarada existe solo en el ambito privado de la clase.
    
    // string - todos los objetos heredados de NSobject necesitan un puntero.
    NSString * nombre;
    
    // entero - los objetos de C no necesitan puntero
    int edad;
    
    // bool - YES - NO
    BOOL casado;
    
    // "IBOutlet" ya que está en el ".xib"
    IBOutlet UILabel * texto;
    
}

// declaración de métodos - por defecto son públivos

// método de instancia

// trabajar();
- (void) trabajar;

// comerDonde(string $comida, bool $restaurant);
- (void) comer : (NSString *) comida donde:(BOOL) restaurant;

// método que se ejecutará al presionar el boton
- (IBAction)presionar:(id)sender;


// métodos estáticos
// se declaran usando el signo " + "

+(void) domir;






@end
