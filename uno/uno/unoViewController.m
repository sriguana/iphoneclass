//
//  unoViewController.m
//  uno
//
//  Created by iPhone on 4/24/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "unoViewController.h"

@implementation unoViewController

- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

// cuando la vista está  cargada en la memoria y disponible paraq presentar la pantalla.

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    nombre = @"Claudio Fuentes";
    edad = 33;
    casado = YES;
    
}

- (void)trabajar {
    
    // %@ string
    // %i int
    // %f float
    // %d double
    // %i bool (YES 1, NO 2)
    
    NSLog(@"%@ está trabajando a los %i años.", nombre, edad);

}

- (void)comer:(NSString *)comida donde:(BOOL)restaurant {
    
    NSLog(@"%@ come %@ en un restaurant? %i", nombre, comida, restaurant);

}

+ (void)domir {
    
    NSLog(@"Duerme como bestia.");
    
}

// método que se ejecuta al presionar le boton
- (IBAction) presionar:(id)sender {
    // asignamos un valor a  la propiedad text de texto.
    texto.text = @"holi";
}


// se ejecuta cuando la vista se ha terminado de mostrar en la pantalla.
// se usa "super" para no sobre escribir el método original

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self trabajar];
    [self comer:@"Porotos" donde:NO];
    [unoViewController domir];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
