//
//  cuatroViewController.h
//  cuatro
//
//  Created by iPhone on 4/26/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
// importando el header (.h) a la clase
#import "CuatroDosViewController.h"
#import "CuatroTresViewController.h"

@interface cuatroViewController : UIViewController {
    
    // instanciando las vistas
    CuatroDosViewController * vistaDos;
    CuatroTresViewController * vistaTres;

}

@end
