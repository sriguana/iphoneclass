//
//  cuatroViewController.m
//  cuatro
//
//  Created by iPhone on 4/26/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "cuatroViewController.h"

@implementation cuatroViewController

- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // asignando valores a las vistas
    vistaDos = [[CuatroDosViewController alloc] initWithNibName:@"CuatroDosViewController" bundle:nil];
    vistaTres = [[CuatroTresViewController alloc] initWithNibName:@"CuatroTresViewController" bundle:nil];
    
    // anidar vistas
    // self.view es la referencia al contenedor principal del controlador actual
    [self.view addSubview:vistaDos.view];
    [self.view addSubview:vistaTres.view];
    
    // animación
    /*
    [UIView animateWithDuration:2 animations:^{
        vistaTres.view.alpha = 0;
        [vistaTres release];
        vistaTres = nil;
        
    } completion:^(BOOL finished) {

        [UIView animateWithDuration:2 animations:^{
            vistaDos.view.alpha = 0;
            [vistaDos release];
            vistaDos = nil;
        }];
    }];
    */
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    vistaDos.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentModalViewController:vistaDos animated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
