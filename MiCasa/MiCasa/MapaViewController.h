//
//  MapaViewController.h
//  MiCasa
//
//  Created by iPhone on 5/8/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "SBJson.h"

@interface MapaViewController : UIViewController {
    IBOutlet MKMapView *mapa;
}

- (NSString *) obtenerCoordenadas: (NSString*) direccion;

@end
