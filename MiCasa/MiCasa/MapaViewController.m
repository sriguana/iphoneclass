//
//  MapaViewController.m
//  MiCasa
//
//  Created by iPhone on 5/8/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "MapaViewController.h"


@implementation MapaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [mapa release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self obtenerCoordenadas:@"Santa Isabel 353, Santiago"];
}

- (void)viewDidUnload
{
    [mapa release];
    mapa = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSString *)obtenerCoordenadas:(NSString *)direccion {
    NSString *google = @"http://maps.googleapis.com/maps/api/geocode/json?address=%@&sensor=true";
    
    NSString *busqueda = [NSString stringWithFormat:direccion];
    
    NSString *dir = [busqueda stringByReplacingOccurrencesOfString:@" " withString:@"+"];
    
    NSString *formatoFinal = [NSString stringWithFormat:google, dir];

    NSURL *url = [NSURL URLWithString:formatoFinal];
    
    NSError *error;
    
    NSString *respuesta = [[NSString alloc] initWithContentsOfURL:url encoding:NSUTF8StringEncoding error:&error];
    
    // NSLog(@"%@", formatoFinal);
    
    SBJsonParser * json = [[SBJsonParser alloc] init];
    
    NSDictionary * resultado = [json objectWithString:respuesta error:&error];
    
    NSDictionary * location = [[[[resultado objectForKey:@"results"] objectAtIndex:0] objectForKey:@"geometry"] objectForKey:@"location"];
    
    NSLog(@"%@", location);
    
    return respuesta;


}

@end
