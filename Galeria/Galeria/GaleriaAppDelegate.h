//
//  GaleriaAppDelegate.h
//  Galeria
//
//  Created by iPhone on 5/17/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GaleriaViewController;

@interface GaleriaAppDelegate : NSObject <UIApplicationDelegate> {

}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain) IBOutlet GaleriaViewController *viewController;

@end
