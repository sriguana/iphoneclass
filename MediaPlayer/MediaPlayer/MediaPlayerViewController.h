//
//  MediaPlayerViewController.h
//  MediaPlayer
//
//  Created by iPhone on 5/15/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface MediaPlayerViewController : UIViewController {
    
    IBOutlet UIView *videoView;
}

@end
