//
//  MediaPlayerViewController.m
//  MediaPlayer
//
//  Created by iPhone on 5/15/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "MediaPlayerViewController.h"

@implementation MediaPlayerViewController

- (void)dealloc
{
    [videoView release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"video_iphone" ofType:@"mov"];
    
    // Archivo local dentro de la app
    // NSURL *url = [NSURL fileURLWithPath:path];
    
    NSURL *url = [NSURL URLWithString:@"http://www.flashfromhell.com/iphone/video_iphone.mov"];
    
    MPMoviePlayerController *video = [[MPMoviePlayerController alloc] initWithContentURL:url];
    
    video.view.frame = videoView.frame;
    
    // usamos un "tag" para acceder al objeto desde afuera del metodo.
    video.view.tag = 5;
    
    [self.view addSubview: video.view];
    
    [video play];
    
}

// antes de rotar entrega la proxima orientacion
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    NSLog(@"%i", toInterfaceOrientation);
    NSLog(@"%f", self.view.frame.size.width / 2);
    NSLog(@"%f", self.view.frame.size.height / 2);
}

// despues de rotar entrega la orientacion anterior
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    
    UIView *videoview = [self.view viewWithTag:5];
    
    videoview.frame = videoView.frame;
    
    // videoview.center = self.view.center;
    
    // videoview.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
    

}


- (void)viewDidUnload
{
    [videoView release];
    videoView = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return YES;
}

@end
