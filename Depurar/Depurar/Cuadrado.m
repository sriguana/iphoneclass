//
//  Cuadrado.m
//  Depurar
//
//  Created by iPhone on 5/17/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "Cuadrado.h"


@implementation Cuadrado

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

// cuando la vista se coloca dentro de una supervista
-(void)didMoveToSuperview {
    
    [NSTimer scheduledTimerWithTimeInterval:1/30 target:self selector:@selector(rotar:) userInfo:nil repeats:YES];
    
}

-(void) rotar:(id)sender {
    
    radianes++;
    self.transform = CGAffineTransformMakeRotation(radianes);
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)dealloc
{
    [super dealloc];
}

@end
