//
//  detalleViewController.m
//  cinco
//
//  Created by iPhone on 5/3/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "detalleViewController.h"


@implementation detalleViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Detalle";
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)irtercera:(id)sender {
    
    // instanciamos la vista detalle (viewController).
    terceraViewController * tercera = [[terceraViewController alloc] initWithNibName:@"terceraViewController" bundle:nil];
    
    [self.navigationController pushViewController:tercera animated:YES];
    [tercera release];
    
}
@end
