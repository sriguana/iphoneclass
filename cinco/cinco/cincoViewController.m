//
//  cincoViewController.m
//  cinco
//
//  Created by iPhone on 5/3/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "cincoViewController.h"

@implementation cincoViewController

- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    inicioViewController * inicio = [[inicioViewController alloc] initWithNibName:@"inicioViewController" bundle: nil];
    NSArray * controllers = [NSArray arrayWithObject: inicio];
    self.viewControllers = controllers;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
