//
//  cincoAppDelegate.h
//  cinco
//
//  Created by iPhone on 5/3/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class cincoViewController;

@interface cincoAppDelegate : NSObject <UIApplicationDelegate> {

}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain) IBOutlet cincoViewController *viewController;

@end
