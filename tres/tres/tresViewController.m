//
//  tresViewController.m
//  tres
//
//  Created by iPhone on 4/26/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "tresViewController.h"

@implementation tresViewController

// "synthesize" crea automaticamente los get y set
@synthesize alumnos;

- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // valores al array alumnos
    // el objeto NSArray una vez definido no puede cambiar jamas
    // solo soporta elemento heredados de NSObject
    alumnos = [[NSArray alloc] initWithObjects:@"Pedro",@"Juan",@"Diego", nil];
    
    // objener elementos del array alumnos
    NSLog(@"%@", [alumnos objectAtIndex:1]);
    NSLog(@"%@", alumnos);
    // liberar variable alumnos de memoria
    [alumnos release];
    alumnos = nil;
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
