//
//  tresAppDelegate.h
//  tres
//
//  Created by iPhone on 4/26/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class tresViewController;

@interface tresAppDelegate : NSObject <UIApplicationDelegate> {

}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain) IBOutlet tresViewController *viewController;

@end
