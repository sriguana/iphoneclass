//
//  dosViewController.h
//  dos
//
//  Created by iPhone on 4/26/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface dosViewController : UIViewController {
    
    IBOutlet UIButton *rojoBoton;
    IBOutlet UIButton *azulBoton;
    IBOutlet UIButton *verdeBoton;
    IBOutlet UIButton *magentaBoton;

}

- (IBAction)onRojo:(id)sender;
- (IBAction)onAzul:(id)sender;
- (IBAction)onVerde:(id)sender;
- (IBAction)onMagenta:(id)sender;

@end





