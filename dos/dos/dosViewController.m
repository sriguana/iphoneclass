//
//  dosViewController.m
//  dos
//
//  Created by iPhone on 4/26/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "dosViewController.h"

@implementation dosViewController

- (void)dealloc
{
    [rojoBoton release];
    [azulBoton release];
    [verdeBoton release];
    [magentaBoton release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    // cambiando el color de fondo a la vista (naranja).
    self.view.backgroundColor = [UIColor orangeColor];
    
}

- (void)viewDidUnload
{
    [rojoBoton release];
    rojoBoton = nil;
    [azulBoton release];
    azulBoton = nil;
    [verdeBoton release];
    verdeBoton = nil;
    [magentaBoton release];
    magentaBoton = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)onRojo:(id)sender {
    // cambiando el color de fondo a la vista (naranja).
    self.view.backgroundColor = [UIColor redColor];
    
}

- (IBAction)onAzul:(id)sender {
    // cambiando el color de fondo a la vista (naranja).
    self.view.backgroundColor = [UIColor blueColor];
}

- (IBAction)onVerde:(id)sender {
    // cambiando el color de fondo a la vista (naranja).
    self.view.backgroundColor = [UIColor greenColor];
}

- (IBAction)onMagenta:(id)sender {
    // cambiando el color de fondo a la vista (naranja).
    self.view.backgroundColor = [UIColor magentaColor];
}
@end
