//
//  SenderViewController.h
//  Sender
//
//  Created by iPhone on 5/15/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

// Se delegan los metodos a alert para poder ejecutar acciones
@interface SenderViewController : UIViewController<UIAlertViewDelegate> {
    
}

-(IBAction) onboton:(id)sender;

@end
