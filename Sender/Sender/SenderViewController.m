//
//  SenderViewController.m
//  Sender
//
//  Created by iPhone on 5/15/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "SenderViewController.h"

@implementation SenderViewController


- (void)onboton:(id)sender {
    // transformar sender en un boton
    
    UIButton * btn = (UIButton *)sender;
    
    NSString * texto = btn.titleLabel.text;
    
    NSLog(@"%@", texto);
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Mi Alerta" message:texto delegate:self cancelButtonTitle:@"Cancelar" otherButtonTitles:@"Si", @"No", nil];
    
    [alert show];
    [alert release];
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    NSLog(@"%i", buttonIndex);
    
}

- (void)dealloc
{
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
}
*/

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
