//
//  navegadorViewController.h
//  navegador
//
//  Created by iPhone on 5/10/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface navegadorViewController : UIViewController <UIWebViewDelegate, UISearchBarDelegate> {

    IBOutlet UISearchBar *buscador;
    IBOutlet UIActivityIndicatorView *cargador;
    IBOutlet UIWebView *navegador;

}

@end
