//
//  navegadorAppDelegate.h
//  navegador
//
//  Created by iPhone on 5/10/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class navegadorViewController;

@interface navegadorAppDelegate : NSObject <UIApplicationDelegate> {

}

@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain) IBOutlet navegadorViewController *viewController;

@end
