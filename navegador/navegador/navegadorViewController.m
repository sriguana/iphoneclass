//
//  navegadorViewController.m
//  navegador
//
//  Created by iPhone on 5/10/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "navegadorViewController.h"

@implementation navegadorViewController

- (void)dealloc
{
    [navegador release];
    [cargador release];
    [buscador release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {

    NSURL * url = [NSURL URLWithString:searchBar.text];
    NSURLRequest * request = [NSURLRequest requestWithURL:url];
    [navegador loadRequest:request];
    [searchBar resignFirstResponder];

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSURL * url = [NSURL URLWithString:@"http:www.google.com/"];
    NSURLRequest * request = [NSURLRequest requestWithURL:url];
    navegador.delegate = self;
    buscador.delegate = self;
    [navegador loadRequest:request];
    cargador.hidesWhenStopped = YES;
    
}

// elementos heredados de UIWebView

-(void)webViewDidStartLoad:(UIWebView *)webView {
    
    // agregando una animación
    /*
    [UIView animateWithDuration:1 animations:^ {
        webView.alpha = 0;
    }];
    */
    [cargador startAnimating];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    // agregando una animación
    /*
    [UIView animateWithDuration:1 animations:^ {
        webView.alpha = 1;
    }];
    */
    [cargador stopAnimating];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
}

- (void)viewDidUnload
{
    [navegador release];
    navegador = nil;
    [cargador release];
    cargador = nil;
    [buscador release];
    buscador = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
